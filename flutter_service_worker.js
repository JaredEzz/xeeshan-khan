'use strict';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "/index.html": "6fe914fbc767f3559984c77c13a76fce",
"/main.dart.js": "1b75b1fedccad98c433f23b1e146ffc0",
"/icons/favicon-16x16.png": "533f88a3fe19ce50de3ab4f9dec60a7f",
"/icons/favicon.ico": "98da0751119641739788d980d67832da",
"/icons/apple-icon.png": "d535bd42240fe958b8ff2845424a964b",
"/icons/apple-icon-144x144.png": "a03190bf6529eb2d7bcd86dae1f1f3f7",
"/icons/android-icon-192x192.png": "93d0595f4eb4c51b98e32ace874f13cf",
"/icons/apple-icon-precomposed.png": "d535bd42240fe958b8ff2845424a964b",
"/icons/apple-icon-114x114.png": "946e65ad3eee9f63696ccd7f6898d1b5",
"/icons/ms-icon-310x310.png": "4b4f6f4d96ddef826e597e8ea18d0403",
"/icons/ms-icon-144x144.png": "a03190bf6529eb2d7bcd86dae1f1f3f7",
"/icons/apple-icon-57x57.png": "136111a6f9fe6167939f325f6eeb7a0d",
"/icons/apple-icon-152x152.png": "cbcc887afa7c4cff54beed31912217df",
"/icons/ms-icon-150x150.png": "742333075765c90fea876882706ae1d9",
"/icons/android-icon-72x72.png": "68f01f94387dab3580594b2e3a44c8e6",
"/icons/android-icon-96x96.png": "1914a25cb2d206966b50713317225bd0",
"/icons/android-icon-36x36.png": "3247236b44789af5d675322a72c3243c",
"/icons/apple-icon-180x180.png": "d633761fd5954d9478491829ed351610",
"/icons/favicon-96x96.png": "1914a25cb2d206966b50713317225bd0",
"/icons/manifest.json": "b58fcfa7628c9205cb11a1b2c3e8f99a",
"/icons/android-icon-48x48.png": "057eaf7cd61dbe9cccea2942bc535f61",
"/icons/apple-icon-76x76.png": "8a5a46694c59737e7e3693644aff0d05",
"/icons/apple-icon-60x60.png": "54ff692cfc3c0f6dd9a0771d24689381",
"/icons/browserconfig.xml": "653d077300a12f09a69caeea7a8947f8",
"/icons/android-icon-144x144.png": "a03190bf6529eb2d7bcd86dae1f1f3f7",
"/icons/apple-icon-72x72.png": "68f01f94387dab3580594b2e3a44c8e6",
"/icons/apple-icon-120x120.png": "5ebb7ece5ac10065868aa38037bcbf58",
"/icons/favicon-32x32.png": "89af7ff036b4c5d9385e5d4bd7c024cd",
"/icons/ms-icon-70x70.png": "2ea7c6f2ed1dc8f05f1b94849de4826c",
"/manifest.json": "a83880f277633e4c495763511788e153",
"/assets/LICENSE": "c77f96f69d1a266922addc309d731bf2",
"/assets/AssetManifest.json": "949352253aa46a82ea383492fc711357",
"/assets/FontManifest.json": "9f12c4d7fa7e129ce6037a08f6bb8662",
"/assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "115e937bb829a890521f72d2e664b632",
"/assets/packages/font_awesome_flutter/lib/fonts/fa-solid-900.ttf": "0ea892e09437fcaa050b2b15c53173b7",
"/assets/packages/font_awesome_flutter/lib/fonts/fa-regular-400.ttf": "d51b09f7b8345b41dd3b2201f653c62b",
"/assets/packages/font_awesome_flutter/lib/fonts/fa-brands-400.ttf": "51d23d1c30deda6f34673e0d5600fd38",
"/assets/packages/icofont_flutter/lib/fonts/icofont.ttf": "22304f677719908079b166a1280db76e",
"/assets/fonts/MaterialIcons-Regular.ttf": "56d3ffdef7a25659eab6a68a3fbfaf16",
"/assets/assets/images/chair3.jpg": "2744331c86d40717e57bf21f75ce5d2c",
"/assets/assets/images/chair2.jpg": "db71c24e311a0261816a3f2a86306de0",
"/assets/assets/images/about_me.png": "b8c718284ad5393a070b62abf16a33ff",
"/assets/assets/images/chair.jpg": "6c522bbfdf92e89c67daf0921b989b6c",
"/assets/assets/images/image2.png": "ddf0b03263b858992c7680365126fc9b",
"/assets/assets/images/image.jpg": "2cdfc39eecc82f1bb7f0f18e5bffe3af",
"/assets/assets/portfolio/did.jpg": "6768b5c80c06d7155272f37318d55e34",
"/assets/assets/portfolio/doctor.jpg": "4f6b3cb1e1d13baf21ac8d771cf6d362",
"/assets/assets/portfolio/Tatweer.jpg": "f454e48b72c3c4db3f061528bf45dc8d",
"/assets/assets/portfolio/reflectly.jpg": "93594227a14d74c7002db712ebef9a33",
"/assets/assets/portfolio/My-Laser.png": "9a879f5c98eed275badacf3588aba5b9",
"/assets/assets/portfolio/homemade.jpg": "eb5607773e34e7dbc9835d46f11463b8",
"/assets/assets/portfolio/ZeConstant.jpg": "ca5bb37dd1dacc8e747bf8e0641cf06d",
"/assets/assets/font/RockoFLF.ttf": "072bff49d3ebbc7543f3796ca611a34f",
"/assets/assets/font/CrimsonText-Regular.ttf": "62f10d46561ed0b911d42b7510d62d74",
"/assets/assets/font/Poppins-Regular.ttf": "41e8dead03fb979ecc23b8dfb0fef627"
};

self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheName) {
      return caches.delete(cacheName);
    }).then(function (_) {
      return caches.open(CACHE_NAME);
    }).then(function (cache) {
      return cache.addAll(Object.keys(RESOURCES));
    })
  );
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        if (response) {
          return response;
        }
        return fetch(event.request, {
          credentials: 'include'
        });
      })
  );
});
